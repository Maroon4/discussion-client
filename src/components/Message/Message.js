import React, {Component} from "react";
import PropTypes, {object} from 'prop-types'
import classNames from 'classnames';

import Time from "../Time";

import './Message.scss'



class Message extends Component {

    state = {
        modal: false,
        addComment: false
    }



    updateMessage = () => {

        this.setState({
            modal: true
        })
        return

    };

    addComment = () => {
        if (this.state.addComment === false) {
            this.setState({
                addComment: true
            })
        } else {
            this.setState({
                addComment: false
            })
        }
    }

    render() {

        const {
            id,
            name,
            text,
            date,
            isComment,
            changeRedirect
        } = this.props;

        console.log(this.props)

        return (
            <div className={classNames("message", {
                "message--iscomment": isComment,

            })
            }>
                <div className="message__content">

                    <div className="message__info">
                        <p className="message__name">
                            <span >{name}</span>
                        </p>
                        <div>
                            <div className="message__bubble">
                                <p className="message__text">{text}</p>
                            </div>
                        </div>

                        {date&& (
                            <span className="message__date">
                      <Time date={date}/>
                   </span>)
                        }
                    </div>
                </div>

            </div>
        )
    }


}



export default Message;