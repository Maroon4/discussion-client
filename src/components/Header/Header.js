import React, {Component} from "react";

import "./Header.scss"
import api from "../../utils/api";


export default class Header extends Component {

    state = {
        messages: []
    };

    getData = async () => {

           await api.getAllMessages().then(messages => {

               // console.log(messages)
                   this.setState({
                       messages: messages.data.data,
                       isLoading: false,
                       redirect: false

                   })
           },
               function (err) {
               if (err.response.status === 404) {
                   return Promise.reject(err.response)
                   // throw new Error('something went wrong')
               }
           }
           )
               .catch(err => {

                   console.log(err)
                   if (err.status === 404) {
                               // return Promise.reject(err.response)
                       return console.log(err.data.error)

                   }
               })

    };

    componentDidMount() {
        this.getData();
    }

    render() {

        const messages = this.state.messages;

       const statisticOfMessages = messages.length;
        // console.log(messages)


        return (
            <div className="row header d-flex">
               <img
                   className="header__img"
                   src="https://freedesignfile.com/upload/2012/09/smilik-2.jpg" />

               <div
                   className="header__statistic"
               >{`Statistic of comments - ${statisticOfMessages}`}</div>

            </div>

        );
    }
}