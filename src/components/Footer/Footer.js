import React, {Component} from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

import "./Footer.scss"



export default class Footer extends Component {

    render() {

        return (
            <MDBFooter color="blue" className="font-small pt-4 mt-4">
                <MDBContainer fluid className="text-center text-md-left">
                    <MDBRow>
                        <MDBCol md="6">
                            <h5 className="title">Автор</h5>
                            <p>
                               Логвинюк Максим
                            </p>
                        </MDBCol>
                        <MDBCol md="6">
                            <h5 className="title">Контакти</h5>
                            <ul>
                                <li className="list-unstyled">
                                    <a >maxlohviniuk41549@gmail.com</a>
                                </li>
                                <li className="list-unstyled">
                                    <a >0674085693</a>
                                </li>
                                <li className="list-unstyled">
                                    <a href="https://www.linkedin.com/in/max-logvynyuk-a970a9189/!">Linkedin</a>
                                </li>
                            </ul>
                        </MDBCol>
                    </MDBRow>
                </MDBContainer>
                <div className="footer-copyright text-center py-3">
                    <MDBContainer fluid>
                        &copy; {new Date().getFullYear()} Copyright: <a > Discussion page </a>
                    </MDBContainer>
                </div>
            </MDBFooter>

        );
    }
}