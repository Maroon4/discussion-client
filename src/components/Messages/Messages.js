import React, {Component} from "react";
import  PropTypes from "prop-types"
import {Empty} from "antd";
import api from "../../utils/api";
import MessageContainer from "../MessageContainer";



class Messages extends Component{

    constructor(props) {
        super(props);
        this.state = {
           messages: [],
           redirect: false,

           isLoading: false,
        }
    }

    changeRedirect = async () => {

       if (this.state.redirect === false) {
           this.setState({
               redirect: true
           });
       } else {
           this.setState({

               redirect: false
           });
       }

          await this.getData();

    };

    getData = async () => {
        await api.getAllMessages().then(messages => {

            this.setState({
                 messages: messages.data.data,
                 isLoading: false,
                 redirect: false
                 })

        },
            function (err) {
            if (err.response.status === 404) {
                return Promise.reject(err.response)
            }
        })
            .catch(err => {

                console.log(err);
                if (err.status === 404) {
                    return console.log(err.data.error)

                }
            }
            )
    };

    componentDidMount = async () => {
        this.setState({
            isLoading: true,
            redirect: false
        });

        await this.getData();

    };


    componentDidUpdate(prevProps) {

        if (this.props.newMessage !== prevProps.newMessage) {

            this.getData();
            console.log("Updeted!!!!!!!!!!!!!!!!!!!!")
        }
    }

    render() {

       console.log(this.state.messages);
       const messages = this.state.messages;

        return (
            !messages.length ? (<Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Коментарі відсутні" />

        ): (
            <div>

                {messages.map(message => {

                    const {_id, name, text, date, iscomment, parentId} = message;

                    // console.log(message)

                    if (iscomment === false && parentId === "null") {
                        return (
                            <MessageContainer
                                messages={messages}
                                key = {_id}
                                id={_id}
                                name={name}
                                text={text}
                                date={date}
                                iscomment={iscomment}
                                changeRedirect={this.changeRedirect}


                            />
                        );
                    }

                })}

            </div>
        )

        )}

}



export default Messages;