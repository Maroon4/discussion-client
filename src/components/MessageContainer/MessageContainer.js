import React, {Component} from "react";
import PropTypes, {object} from 'prop-types'
import {Button, Input} from "antd";


import Message from "../Message";
import {ChatInputComment} from "../ChatInput";
import api from "../../utils/api";

import './MessageContainer.scss'
import './MessageContainer.scss'
import MessageComment from "../MessageComment";




const MyButton = ({onClick}) => {
    return (
        <Button
        onClick={onClick}
        >Відправити</Button>
    );
}

class DeleteMessage extends Component {
    deleteUser = event => {
        event.preventDefault();

        if (
            window.confirm(
                `Do tou want to delete the order ${this.props.id} permanently?`,
            )
        ) {
            api.deleteMessageById(this.props.id);
            window.location.reload()
        }
    };

    render() {
        return <div className="messagecontainer__button-delete">
            <Button
                type="danger"

                onClick={this.deleteUser}>Delete</Button>
        </div>
    }
}


class Modal extends Component{

    state = {
        id: '',
        name: '',
        date: '',
        text: '',
        // comments: [],
        iscomment: false,



    }

    handleChangeInputText = async event => {
        const text = event.target.value;

        this.setState({
            text: text
        })
    };

    handleUpdateMessage = async () => {

        const {
            id,
            name,
            date,
            text,
            iscomment} = this.state;

        const payload = { name,
            date,
            text, iscomment};


        await api.updateMessageById(id, payload).then(res => {
            window.alert(`Message inserted successfully`);
            window.location.reload()


            this.setState({
                id: this.props.id,
                name: '',
                date: '',
                text: '',
                iscomment: '',
            })
        });
    };

    getData = async () => {
        await api.getMessageById(this.props.id).then(message => {

            console.log(this.props.id)

            this.setState({
                id: this.props.id,
                name: message.data.data.name,
                date: message.data.data.date,
                text: message.data.data.text,
                iscomment: false,

            })
        })
    };


    componentDidMount() {
         this.getData()
    }

    render() {

        const {name, text, } = this.state;

        return (

            <div>
                <div
                    className="modal-body chat-input"
                >
                 <p><b>Редагувати повідомлення</b></p>
                    <div>
                        <span>{name}</span>
                    </div>
                    <div>
                        <Input className="chat-input__text"
                               type="text"
                               value={text}
                               onChange={this.handleChangeInputText}
                               size="large"
                        />
                    </div>
                    <Button
                        type="primary"
                        onClick={
                            this.handleUpdateMessage}
                    >Відправити</Button>
                </div>

            </div>
        );
    }

}


class MessageContainer extends Component{

  state = {
      messageId: this.props.id,
      messages: this.props.messages,
      modal: false,
      addComment: false
  }



    updateMessage = () => {

      this.setState({
          modal: true
      })
        return

    };

   addComment = () => {
       if (this.state.addComment === false) {
           this.setState({
               addComment: true
           })
       } else {
           this.setState({
               addComment: false
           })
       }
   }



    render() {

    const {modal, addComment, messageId} = this.state;

    const {messages, id, name, text, date, iscomment, changeRedirect} = this.props;

    console.log(messages);

    return(

        <div>

            {modal === true ? <Modal id={id}/> : (
                <div
                    className="messagecontainer">
                    <div className=" messagecontainer__button-div">
                        <div className=" messagecontainer__button-div-button-delete">
                            <DeleteMessage
                                id={id}
                            >Видалити</DeleteMessage>
                        </div>
                        <div  className=" messagecontainer__button-div-button-edit">
                            <Button
                                onClick={this.updateMessage}
                            >Редагувати</Button>
                        </div>
                    </div>
                    <Message
                        id={id}
                        name={name}
                        text={text}
                        date={date}
                    />
                    <div >
                        <div>


                            <span className="messagecontainer__comment-header" >Залишити коментар</span>
                            <ChatInputComment
                                id={id}
                                iscomment={true}
                                changeRedirect={changeRedirect}
                                addComment={this.addComment}

                            />
                        </div>
                        <p>Коментарі</p>
                        {
                            messages.map( comment => {

                            console.log(messages)

                            console.log(comment)

                            console.log(id)

                                console.log(comment.parentId)


                              if(comment.iscomment === true && comment.parentId === id)

                              {
                                  const {
                                      _id,
                                      name,
                                      text,
                                      date,
                                      iscomment,
                                      parentId
                                  } = comment;

                                  console.log(_id)
                                  console.log(`${name}`)

                                  console.log(comment._id)
                                  return (

                                      <div key={_id} >
                                          <MessageComment
                                              key={_id}
                                              id={_id}
                                              name={name}
                                              text={text}
                                              date={date}
                                              isсomment={iscomment}
                                              changeRedirect={changeRedirect}
                                              messages={messages}
                                              parentId={parentId}
                                          />
                                      </div>
                                  )
                              }


                          }

                        )

                        }
                    </div>


                </div>
            ) }
        </div>)

  }

}


export default MessageContainer;