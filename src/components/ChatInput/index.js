import ChatInput from "./ChatInput";
import ChatInputComment from "./ChatInputComment";


export {
    ChatInput,
    ChatInputComment
}