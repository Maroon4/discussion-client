import React, {Component} from "react";
import PropTypes from "prop-types"
import {Button, Input} from "antd"

import api from "../../utils/api";


import "./ChatInput.scss"

class ChatInputComment extends Component {

    constructor(props) {
        super(props)

        const randonId = Math.floor(Math.random()*10000) + 2;

        this.state = {

            name: '',
            date: new Date(),
            text: '',
            iscomment: true,
            parentId: `${this.props.id}`,

        }
    }




    handleChangeInputName = async event => {
        const name = event.target.value;

        // console.log(name);
        this.setState({
                name: name,
        });

    };

    handleChangeInputText = async event => {
        const text = event.target.value;


        this.setState({
                text: text,
        })
    };


    handleUpdateMessage = async () => {

       const {changeRedirect} = this.props;


        const {
            // id,
            name,
            date,
            text,
            iscomment,
            parentId } = this.state;

        const payload = { name,
            date,
            text, iscomment, parentId};



        console.log(payload);

        await api.insertMessage(payload).then(res => {

            this.setState({
                name: '',
                date: new Date(),
                text: '',
                comments: [],
                iscomment: true,
                parentId: `${this.props.id}`,
            })
        });
        changeRedirect();
        this.props.addComment();

    };

    componentDidMount = async () => {
        const { id } = this.state;
        const message = await api.getMessageById(id);

        // console.log(message);

        this.setState({
            name: message.data.data.name,
            date: message.data.data.date,
            text: message.data.data.text,
            comments: message.data.data.comments,
            iscomment: message.data.data.iscomment

        })
    };



    render() {

       const {changeRedirect} = this.props;
       const {name, text} = this.state

        return (
            <div
                // onChange={this.refreshComments}
                className="chat-input">
                <div className="chat-input__name">
                    <p className="chat-input__name-header">Введіть імʼя</p>
                    <Input
                        onChange={this.handleChangeInputName}
                        size="large"
                        value={name}

                    />
                </div>
                <div className="chat-input__text">
                    <Input className="chat-input__text"
                           onChange={this.handleChangeInputText}
                           size="large"
                           value={text}
                    />
                    <div className="chat-input__button-send">

                        <Button
                            type="primary"
                            onClick={
                                this.handleUpdateMessage}
                        >Відправити</Button>

                    </div>
                </div>


            </div>
        );
    }


}

ChatInputComment.propTypes = {
    className: PropTypes.string
};

export default ChatInputComment;
