import React, {Component} from "react";
import PropTypes from "prop-types"
import {Button, Input} from "antd"

import api from "../../utils/api";


import "./ChatInput.scss"

class ChatInput extends Component {

    constructor(props) {
        super(props)

        this.state = {
            name: '',
            date: new Date(),
            text: '',
            comments: [],
            iscomment: this.props.iscomment,
            parentId: 'null',
            changeRedirect: false

        }
    }



    handleChangeInputName = async event => {
        const name = event.target.value;
        console.log(name);
        this.setState({ name })
    };

    handleChangeInputText = async event => {
        const text = event.target.value;
        this.setState({ text })
    };

    handleIncludeMessage = async () => {

        const {
            name,
            date,
            text, comments, iscomment, parentId } = this.state;


        const payload = { name,
            date,
            text, comments, iscomment, parentId};

        this.setState({
            name: '',
            date: new Date(),
            text: '',
            comments: [],
            iscomment: false,
            parentId: 'null',
            // changeRedirect: true
        });

        console.log(this.state);

        await api.insertMessage(payload).then(res => {
            // window.alert(`Order inserted successfully`);

            this.setState({
                name: '',
                date: new Date(),
                text: '',
                comments: [],
                iscomment: false,
                parentId: 'null',
                // changeRedirect: false
            })
        })

        this.props.setNewMessage();

        this.setState({
            name: '',
            date: new Date(),
            text: '',
            comments: [],
            iscomment: this.props.iscomment,
            parentId: 'null',
            changeRedirect: false
        })
    };

    updateState = () => {
        this.setState({
            // name: '',
            // text: '',
            // changeRedirect: false
        })
    };

    componentDidMount() {
       // this.updateState();
    }


    componentDidUpdate(prevState){
        if(this.state.changeRedirect !== prevState.changeRedirect) {
           // this.updateState();

            console.log("Input update!")
        }
    }


    render() {

        const {setNewMessage} = this.props;

        const {name, text} = this.state;

        return (
            <div className="chat-input">
                <div className="chat-input__name">
                    <p className="chat-input__name-header">Введіть імʼя</p>
                    <Input
                        onChange={this.handleChangeInputName}
                        size="large"
                        // placeholder={name}

                        value={name}
                    />
                </div>
                <div className="chat-input__text">
                    <Input className="chat-input__text"
                           onChange={this.handleChangeInputText}
                           size="large"
                           // placeholder={text}
                           value={text}
                    />
                    <div className="chat-input__button-send">

                        <Button
                            type="primary"
                            onClick={this.handleIncludeMessage}
                        >Відправити</Button>

                    </div>
                </div>


            </div>
        );
    }


};

ChatInput.propTypes = {
    className: PropTypes.string
};

export default ChatInput;
