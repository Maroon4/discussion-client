import React, {Component} from "react";

import Messages from "../../components/Messages";
import {ChatInput} from "../../components/ChatInput";
import Footer from "../../components/Footer";

import "./Home.scss";
import "../../styles/layouts/_chat.scss"



class Home extends Component {

  constructor(props) {
      super(props);

      this.state = {
          newMessage: false
      }

  }

  setNewMessage = () => {

      if (this.state.newMessage === false) {
          this.setState({
              newMessage: true
          })
      } else {
          this.setState({
              newMessage: false
          })
      }
  };


  render() {
      return (
          <section className="home">

              <div className="chat">
                  <div className="chat__dialog">
                      <div className="chat__dialog-header">
                          <div className="chat__dialog-header-center">
                              <b className="chat__dialog-header-username">Діалог</b>
                          </div>
                      </div>
                      <div className="chat__dialog-messages">
                          <Messages
                          newMessage={this.state.newMessage}
                          />

                      </div >
                      <div className="chat-input">
                          <ChatInput
                              setNewMessage={this.setNewMessage}
                              iscomment={false}
                          />
                      </div>
                      <Footer/>
                  </div>
              </div>

          </section>
      );
  }

}

export default Home;