import axios from 'axios'
import runtimeEnv from '@mars/heroku-js-runtime-env';

const env = runtimeEnv();

const api = axios.create({
    baseURL: 'https://maroon-discussion-page-server.herokuapp.com/api',
    // baseURL: env.API_URL
    // baseURL:'http://localhost:3000/api'
});

export const insertMessage = payload => api.post(`/message`, payload);
export const getAllMessages = () => api.get(`/messages`);
export const updateMessageById = (id, payload) => api.put(`/message/${id}`, payload);
export const deleteMessageById = id => api.delete(`/message/${id}`);
export const getMessageById = id => api.get(`/message/${id}`);




const apis = {
    insertMessage,
    getAllMessages,
    updateMessageById,
    deleteMessageById,
    getMessageById
};

export default apis





