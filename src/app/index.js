import React from 'react';
import runtimeEnv from '@mars/heroku-js-runtime-env'

import "./App.scss"

import Header from "../components/Header";
import Home from "../pages/Home/Home";


// import 'bootstrap/dist/css/bootstrap.min.css'

const App = () => {

 // const env = runtimeEnv();

  return (


       <div className="app">
           <Header/>
           <Home/>

       </div>

  );
};

export default App;
